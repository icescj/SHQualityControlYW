package com.example.sinoyd.frameapplication.KotlinFrame.Code.UI

import android.content.Intent
import android.os.Bundle
import com.example.sinoyd.frameapplication.KotlinFrame.UI.BaseActivity
import com.example.sinoyd.frameapplication.R
import kotlinx.android.synthetic.main.activity_new_temporary_tasks_.*


/**新建临时任务**/
class New_Temporary_Tasks_Activity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_temporary_tasks_)
        //设置监听
        setlisteners()
    }

    private fun setlisteners() {


        newSamplingrecord.setOnClickListener {
            startActivity(Intent(this@New_Temporary_Tasks_Activity,New_Sampling_record_Activity::class.java))
        }
        //水源地水质自动
        newWaterqualityanto.setOnClickListener {
            startActivity(Intent(this@New_Temporary_Tasks_Activity,New_Water_quality_anto_Activity::class.java))
        }
        //盲样考核
        newBlindnessexamination.setOnClickListener {
            startActivity(Intent(this@New_Temporary_Tasks_Activity,New_Blindness_examination_Activity::class.java))
        }


        /**************************************************************/
        //实样比对
        newRealsamplecomparison.setOnClickListener {
            var intent= Intent(this@New_Temporary_Tasks_Activity,New_Realsample_comparison_Activity::class.java)
            intent.putExtra("rowGuid","新建临时任务")
            startActivity(intent)
         }
        //水质巡检
        newwaterMonitoringinspection.setOnClickListener {
            var intent= Intent(this@New_Temporary_Tasks_Activity,New_Water_Monitoring_Inspection_Activity::class.java)
            intent.putExtra("rowGuid","新建临时任务")
            startActivity(intent)
         }
        //标样核查
        newStandardsolutionverification.setOnClickListener {
            var intent= Intent(this@New_Temporary_Tasks_Activity,New_Standard_solution_verification_Activity::class.java)
            intent.putExtra("rowGuid","新建临时任务")
            startActivity(intent)
        }
        //加标回收
        newStandardRecovery.setOnClickListener {
            var intent= Intent(this@New_Temporary_Tasks_Activity,New_Standard_Recovery_Activity::class.java)
            intent.putExtra("rowGuid","新建临时任务")
            startActivity(intent)
        }
        //性能考核
        newPerformanceassessment.setOnClickListener {
            var intent= Intent(this@New_Temporary_Tasks_Activity,New_Performance_assessment_Activity::class.java)
            intent.putExtra("rowGuid","新建临时任务")
            startActivity(intent)
        }
    }
}
