package com.example.sinoyd.frameapplication.KotlinFrame.Code.UI

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.sinoyd.frameapplication.R
import kotlinx.android.synthetic.main.activity_reagent_standard_liquid_configuration_.*

/**试剂标液管理**/
class Reagent_reducatant_manager_Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reagent_standard_liquid_configuration_)
        //设置监听
        setlisteners()
    }

    private fun setlisteners() {
        //试剂管理
        ll_reagent.setOnClickListener {
            startActivity(
                Intent(
                    this@Reagent_reducatant_manager_Activity,
                    Reagent_manager_Activity::class.java
                )
            )
        }

        //标液管理
        ll_reductant.setOnClickListener {
            startActivity(
                Intent(
                    this@Reagent_reducatant_manager_Activity,
                    Reductant_manager_Activity::class.java
                )
            )
        }
    }
}
