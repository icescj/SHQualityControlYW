package com.example.sinoyd.frameapplication.KotlinFrame.Code.UI

import android.content.Intent
import android.os.Bundle
import com.example.sinoyd.frameapplication.R
import kotlinx.android.synthetic.main.activity_sign_in_.*
import com.google.zxing.integration.android.IntentIntegrator
import android.util.Log
import android.widget.Toast
import com.example.sinoyd.frameapplication.KotlinFrame.UI.BaseActivity

/**签到**/
class Sign_in_Activity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in_)
        //设置监听
        setlisteners()
    }

    private fun setlisteners() {
        //返回主页
        iv_home.setOnClickListener {
            finish()
        }
        //扫描
        iv_scan.setOnClickListener {
            val integrator = IntentIntegrator(this@Sign_in_Activity)
            // 设置要扫描的条码类型，ONE_D_CODE_TYPES：一维码，QR_CODE_TYPES-二维码
            integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES)
            integrator.captureActivity = ScanActivity::class.java
            integrator.setPrompt("请扫描二维码") //底部的提示文字，设为""可以置空
            integrator.setCameraId(0) //前置或者后置摄像头
            integrator.setBeepEnabled(true) //扫描成功的「哔哔」声，默认开启
            integrator.setBarcodeImageEnabled(false)//是否保留扫码成功时候的截图
            integrator.initiateScan()
        }
    }

    //扫描返回的结果
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 0) {
            Toast.makeText(this@Sign_in_Activity, "未进行扫描操作", Toast.LENGTH_LONG).show()
        } else {
            val scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
            if (scanResult != null) {
                val result = scanResult.contents
                Log.e("扫描结果", result)
                Toast.makeText(this@Sign_in_Activity, result, Toast.LENGTH_LONG).show()
            }
        }

    }
}
