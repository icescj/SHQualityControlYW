package com.example.sinoyd.frameapplication.KotlinFrame.Code.db;

import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;
import org.xutils.db.annotation.Column;
import org.xutils.db.annotation.Table;

/**
 * 作者： scj
 * 创建时间： 2018/5/9
 * 版权： 江苏远大信息股份有限公司
 * 描述： com.example.sinoyd.frameapplication.KotlinFrame.Code.db
 */

@Table(name = "FormTaskFactor")
public class FormTaskFactor implements IFormTaskFactor {
    @Column(name = "id", isId = true)
    private int id;//id索引
    @Column(name = "TaskGuid")
    private String TaskGuid = "";//任务id
    @Column(name = "PollutantCode")
    private String PollutantCode = "";//因子编号
    @Column(name = "PollutantValue")
    private String PollutantValue = "";//因子值
    @Column(name = "PollutantName")
    private String PollutantName = "";//因子名称
    @Column(name = "unit")
    private String unit = "";//因子单位
    @Column(name = "StanLiquidCode")
    private String StanLiquidCode = "";//标液编号
    @Column(name = "DilutionMethod")
    private String DilutionMethod = "";//稀释方式
    @Column(name = "RemainderUnit")
    private String RemainderUnit = "";//用量单位
    @Column(name = "RemainderValue")
    private String RemainderValue;//用量值
    @Column(name = "StaLiquidGuid")
    private String StaLiquidGuid = "";//标液编号


    @NotNull
    @Override
    public String getRemainderValue() {
        return RemainderValue;
    }

    @Override
    public void setRemainderValue(String remainderValue) {
        RemainderValue = remainderValue;
    }

    public String getPollutantName() {
        return PollutantName;
    }

    public void setPollutantName(String pollutantName) {
        PollutantName = pollutantName;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getPollutantValue() {
        return PollutantValue;
    }

    public void setPollutantValue(String pollutantValue) {
        PollutantValue = pollutantValue;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTaskGuid() {
        return TaskGuid;
    }

    public void setTaskGuid(String taskGuid) {
        TaskGuid = taskGuid;
    }

    public String getPollutantCode() {
        return PollutantCode;
    }

    public void setPollutantCode(String pollutantCode) {
        PollutantCode = pollutantCode;
    }


    public String getStanLiquidCode() {
        return StanLiquidCode;
    }

    public void setStanLiquidCode(String stanLiquidCode) {
        StanLiquidCode = stanLiquidCode;
    }

    public String getDilutionMethod() {
        return DilutionMethod;
    }

    public void setDilutionMethod(String dilutionMethod) {
        DilutionMethod = dilutionMethod;
    }

    public String getRemainderUnit() {
        return RemainderUnit;
    }

    public void setRemainderUnit(String remainderUnit) {
        RemainderUnit = remainderUnit;
    }

    public String getStaLiquidGuid() {
        return StaLiquidGuid;
    }

    public void setStaLiquidGuid(String staLiquidGuid) {
        StaLiquidGuid = staLiquidGuid;
    }


}
