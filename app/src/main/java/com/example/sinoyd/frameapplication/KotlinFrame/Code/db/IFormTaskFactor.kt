package com.example.sinoyd.frameapplication.KotlinFrame.Code.db

/**
 * 作者： scj
 * 创建时间： 2018/5/11
 * 版权： 江苏远大信息股份有限公司
 * 描述： com.example.sinoyd.frameapplication.KotlinFrame.Code.db
 */


interface IFormTaskFactor {
    //任务id
    var TaskGuid: String
    //因子编号  英文名
    var PollutantCode: String
    //因子值[测试值]
    var PollutantValue: String
    //因子名称
    var PollutantName: String
    //单位
    var unit: String

    /***下面是个是标样需要的参数***/
    //标液编号
    var StanLiquidCode: String
    //稀释方式
    var DilutionMethod: String
    //使用量单位
    var RemainderUnit: String
    //使用量值
    var RemainderValue: String
    //标液编号Guid
    var StaLiquidGuid: String

}
