package com.example.sinoyd.frameapplication.KotlinFrame.Frame.Uitl

import android.util.Log
import com.sinoyd.Code.Until.HttpListener
import okhttp3.*
import java.io.IOException

/**
 * 作者： scj
 * 创建时间： 2018/5/12
 * 版权： 江苏远大信息股份有限公司
 * 描述： com.example.sinoyd.frameapplication.KotlinFrame.Code.Mode
 */


class Networkrequestmodel {

    companion object {
        val GETREQUEST = "GET"
        val POSTREQUEST = "POST"
    }

    var parameter = HashMap<String, String>()
    var parameterrul = ""
    var tag = ""
    var url = ""
    var Method = ""
    var okHttpClient = OkHttpClient()

    init {
        parameter = HashMap()
        parameterrul = ""
        okHttpClient = OkHttpClient()
    }

    fun setMethod(method: String): Networkrequestmodel {
        this.Method = method
        return this
    }

    fun settag(tag: String): Networkrequestmodel {
        this.tag = tag
        return this
    }

    fun seturl(url: String): Networkrequestmodel {
        this.url = url
        return this
    }

    fun addparam(key: String, value: String): Networkrequestmodel {
        parameter.put(key, value)
        return this
    }


    private fun setparameter() {
        for (item in parameter) {
            parameterrul += "&${item.key}=" + item.value
        }
    }

    fun start(listener: HttpListener) {
        var request: Request? = null
        when (Method) {
        //GET请求方式
            GETREQUEST -> {
                setparameter()
                request = Request.Builder()
                        .url(url + "?" + parameterrul.substring(1, parameterrul.length))
                        .build()
            }
        //Post请求方式
            POSTREQUEST -> {
                var formBody = FormBody.Builder()
                //设置参数名称和参数值
                for (item in parameter) {
                    formBody.add(item.key, item.value)
                }
                request = Request.Builder()
                        .url(url)
                        .post(formBody.build())
                        .build()
            }
        }
        //开始发送请求
        try {
            var response = okHttpClient.newCall(request).enqueue(object : Callback {
                override fun onFailure(call: Call?, e: IOException?) {
                    listener.onFailure(call!!)
                }

                override fun onResponse(call: Call?, response: Response) {
                    if (response.isSuccessful) {
                        listener.requestSuccess(response, tag)
                    } else {
                        listener.requestFailed(response)
                    }
                }

            })
        } catch (e: Exception) {
            Log.i("scj", "网络请求模块中异常了" + e.message)
            e.printStackTrace()
        }


    }

}